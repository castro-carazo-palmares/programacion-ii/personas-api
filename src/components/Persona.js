import React, { Component } from "react";
import './Persona.scss';

class Persona extends Component {
    constructor() {
        super();
        this.state = {
            personas: []
        };

        this.submitHandler = this.submitHandler.bind(this);
        this.buildURLQuery = this.buildURLQuery.bind(this);
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        let options = {
            method: "GET",
            headers: {
                Authentication: "CastroCarazo"
            }
        };

        fetch("http://localhost:8080/persona", options)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ personas: data });
            })
            .catch(error => {
                console.log("Hubo un problema con la petición Fetch: " + error.message);
            });
    };

    buildURLQuery = obj =>
        Object.entries(obj)
            .map(pair => pair.map(encodeURIComponent).join("="))
            .join("&");

    submitHandler = event => {
        event.preventDefault();

        let persona = {
            cedula: event.target[0].value,
            nombre: event.target[1].value,
            apellido: event.target[2].value,
            ciudad: event.target[3].value
        };
        
        event.target.reset();

        let options = {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authentication': "CastroCarazo"
            }
        };
        
        fetch("http://localhost:8080/persona?" + this.buildURLQuery(persona), options)
            .then(response => {
                if (response.ok) {
                    console.log("La persona fue insertada correctamente");
                }
            })
            .then(() => this.getData() )
            .catch(error => {
                console.log("Hubo un problema con la petición Fetch: " + error.message);
            });
    };

    render() {
        const { personas } = this.state;

        return (
            <div className="container">
                <div className="formulario row">
                    <form onSubmit={this.submitHandler}>
                        <label htmlFor="cedula">Cédula</label>
                        <input type="number" name="cedula" required />

                        <label htmlFor="nombre">Nombre</label>
                        <input type="text" name="nombre" required />

                        <label htmlFor="apellido">Apellido</label>
                        <input type="text" name="apellido" required />

                        <label htmlFor="ciudad">Ciudad</label>
                        <input type="text" name="ciudad" required />
                        <input type="submit" value="Enviar" />
                    </form>
                </div>
                <div className="lista row">
                    {personas.map(persona => (
                        <div key={persona.cedula} className="persona col-12 col-md-4">
                            <div className="info">
                                <h2>{persona.cedula}</h2>
                                <h4>{persona.nombre}</h4>
                                <h4>{persona.apellido}</h4>
                                <h4>{persona.ciudad}</h4>
                            </div>
                        </div>
                    ))}
                </div>                
            </div>
        );
    }
}

export default Persona;
